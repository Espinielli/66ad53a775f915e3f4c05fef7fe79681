---
title: "Polygon are in R with sf"
author: "Enrico Spinielli"
date: "October 15, 2017"
output:
  html_document:
    keep_md: TRUE
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


Source `polygon_area.R` and you will get the answer in ${km}^2$.

There are two CSV files for the polygons.
Polygons are "closed", i.e. first and last point are the same.

The area of the full polygon is $22597969\ {km}^2$
while the area of the simplified one is $27412031\ {km}^2$.

Here is the map of the two (red = full polygon, blue = simple polygon).

![](polys.png)